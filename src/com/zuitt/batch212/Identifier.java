package com.zuitt.batch212;

public class Identifier {

    public static void main(String[] args) {
        // Identifiers act as containers for values that are to be used by the program and manipulated further as needed.

        // identifier declaration
        int myNum;

        // initialization
        // literals/constant - any constant value which can be assigned to the identifier
        int x = 100;

        myNum = 29;
        System.out.println(myNum);

        // reassign
        myNum = 1;
        System.out.println(myNum);

        int age;
        char middle_name;

        // constant
        final int PRINCIPAL = 1000;
        // PRINCIPAL = 500; cannot assign a value to a final
        System.out.println(PRINCIPAL);

        // primitive data types
        char letter = 'A';
        boolean isMarried = false;
        byte students = 127;
        short seats = 32767;
        int localPopulation = 2_146_273_827;
        // _ will not affect the code
        System.out.println(localPopulation);
        long worldPopulation = 2_146_273_827L;
        // L - long literals

        // decimal points
        float price = 12.99F;
        double temperature = 12745.43254;

        // to grab data type of an identifier we can use getClass()
        System.out.println(((Object)temperature).getClass());

        // non primitive data type / referencing data types
        // string, arrays, classes, interface

        String name = "John Doe";
        System.out.println(name);

        String editedName = name.toLowerCase();
        System.out.println(editedName);

        System.out.println(name.getClass());

        // escape characters (\n, \")
        System.out.println("c:\\windows\\desktop");

        // typecasting - the process of converting of data types

        // implicit casting / widening type casting - automatic conversion
        // triggered if the conversion involves a smaller data type to the larger type
        // byte -> short -> int -> long -> float -> double

        // convert byte(smaller) to double(larger)
        byte num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println(total);

        // explicit casting / narrowing type casting - manual conversion
        // if larger size to smaller size data type
        // converting double to int
        int num3 = 6;
        double num4 = 7.4;
        int anotherTotal = (int) (num3 + num4);
        System.out.println(anotherTotal);

        // concatenation
        String mathGrade = "90";
        String englishGrade = "85";
        System.out.println(mathGrade + englishGrade);

        // convert string to integers
        int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
        System.out.println(totalGrade);

        // converting integers into string
        String stringGrade = Integer.toString(totalGrade);
        System.out.println(stringGrade.getClass());


    }

}
