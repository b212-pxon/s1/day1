package com.zuitt.batch212;

public class HelloWorld {

    public static void main (String[] args){
        System.out.println("Hello World");
    }

    // public - access modifiers (who can see this method)
    // static - non-access modifier (how should this method behave)
    // void - return type (what should this method return?)
    // main - method name (what should this method be called?)
    // String[] args - method parameters (what is needed by method to run)

}
